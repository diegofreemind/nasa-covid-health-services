/**
 * Connect the application to hyperledger fabric
 * Gateway - act as an abstraction between application and network details
 * Wallet - holds a set of user identities
 *
 * Reference:
 * https://hyperledger-fabric.readthedocs.io/en/release-1.4/developapps/designelements.html
 */

const env = require('../config/env');
const connectionProfile = require('../config/network');
const { InMemoryWallet, Gateway } = require('fabric-network');

//https://fabric-sdk-node.github.io/release-1.4/module-fabric-network.InMemoryWallet.html
const wallet = new InMemoryWallet();

//network + contract target
async function getContract(gateway, contractId) {

    try {

        const network = await gateway.getNetwork(env.channel || 'mychannel');
        const contract = await network.getContract(contractId);

        return contract;

    } catch (err) {

        console.log('Error getting contract', err.stack);
        return new Error(`Error getting contract ${err}`);
    }

}

//join all elements to connect into network
async function connect(profile, options) {

    try {

        const gateway = new Gateway();
        await gateway.connect(profile, options);

        return gateway;

    } catch (err) {

        return new Error(err.message);
    }
}

//disconnect this gateway from network
function disconnect(gateway) {

    gateway.disconnect();
}

//retrieve the user context to perform this request
async function context(user) {

    const { identity, certificates } = user;

    let connectionOptions = {
        wallet: wallet,
        identity: identity,
        discovery: { enabled: false, asLocalhost: true }
    };

    try {

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists(identity);
        const identityKeys = JSON.parse(certificates);

        if (!userExists) {

            console.log(`Importing user ${identity} into this wallet`, typeof identityKeys, typeof certificates);
            await wallet.import(identity, identityKeys);
        }

        //throw any already loaded session ( disconnect in every request )

        const gateway = await connect(connectionProfile, connectionOptions);

        const contract = await getContract(gateway, env.chaincode);
        return { contract, gateway };

    } catch (err) {

        //throws an error if connection to network fails
        console.log(err.stack);
        throw new Error(err.message);
    }

};

module.exports = {
    connect,
    context,
    disconnect,
    getContract
}