require('dotenv').config();

if (!process.env.ENROLLMENT_SECRET || !process.env.ENROLLMENT_ID) {
    throw new Error('Missing identity configurations');
}

module.exports = {

    mongodb: {
        host: process.env.MONGO_HOST,
        database: process.env.MONGO_DB,
        api_key: process.env.MONGO_API_KEY
    },
    server_port: process.env.PORT,
    channel: process.env.DEFAULT_CHANNEL,
    chaincode: process.env.DEFAULT_CONTRACT,
    enrollment_id: process.env.ENROLLMENT_ID,
    fabric_ca_host: process.env.FABRIC_ADMIN_HOST,
    identity: {
        role: process.env.DEFAULT_ROLE,
        attrs: [{
            name: process.env.DEFAULT_ATTR_NAME,
            value: process.env.DEFAULT_ATTR_VALUE,
            ecert: true
        }],
        enroll_id: process.env.ENROLLMENT_ID,
        maxEnrollments: Number(process.env.MAX_ENROLLMENTS),
        affiliation: process.env.DEFAULT_AFFILIATION
    },
    identity_org: `${process.env.ENROLLMENT_ID}@${process.env.ORGANIZATION}`
}
