const chai = require('chai');
const queryPayload = require('./query');
const invokePayload = require('./invoke');
const context = require('../providers/context');
const { InMemoryWallet } = require('fabric-network');
const { certificates, identity } = require('./keys');
const connectionProfile = require('../config/network');
const controller = require('../controller/api-controller');

const assert = chai.assert;
const wallet = new InMemoryWallet();

describe('Connect to hyperledger fabric network', function () {

    const user = {
        identity,
        certificates
    };

    describe(`context()`, function () {

        const { identity, certificates } = user;

        let connectionOptions = {
            wallet: wallet,
            identity: identity,
            discovery: { enabled: false, asLocalhost: true }
        };

        let gateway = null;
        let contract = null;

        it(`#wallet.exists()`, async () => {

            try {

                // Check to see if we've already enrolled the user.
                const userExists = await wallet.exists(identity);

                assert.isBoolean(userExists);
                return `boolean ${userExists}`

            } catch (error) {
                return error;
            }
        });

        it(`#wallet.import()`, async () => {

            try {

                await wallet.import(identity, certificates);
                return `imported ${identity}`

            } catch (error) {
                return error;
            }
        });

        it(`#connect()`, async () => {

            try {

                gateway = await context.connect(connectionProfile, connectionOptions);
                assert.isObject(gateway);

            } catch (error) {
                return error;
            }
        });

        it(`#getContract()`, async () => {

            try {

                contract = await context.getContract(gateway, 'fabcar');
                assert.isObject(contract);

            } catch (error) {
                return error;
            }
        });

        it(`#invoke()`, async () => {

            try {

                certificates = JSON.stringify(user.certificates);
                const invokeResult = await controller.invoke(invokePayload, { certificates, identity });

                console.log(`invoke ${invokeResult}`);
                assert.isNotNull(invokeResult);

            } catch (error) {
                return error;
            }
        });

        it(`#query()`, async () => {

            try {

                certificates = JSON.stringify(user.certificates);
                const queryResult = await controller.query(queryPayload, { certificates, identity });

                console.log(`query ${queryResult}`);
                assert.isNotNull(queryResult);

            } catch (error) {
                return error;
            }
        });

    });
});