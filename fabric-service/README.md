A micro service to manage covid-19 cases
[<img src="https://hyperledger-fabric.readthedocs.io/en/latest/_images/hyperledger_fabric_logo_color.png" align="right"
alt="Discover the hyperledger project">](https://hyperledger-fabric.readthedocs.io/en/latest/)
==================

This is a node rest-api template for handling assets in hyperledger fabric network.
It must be customized to your needs, offering a boilerplate to connect your micro-services to blockchains
networks.

[From hyperledger-fabric.readthedocs.io ](https://hyperledger-fabric.readthedocs.io/en/latest/fabric_model.html#assets):

"Assets can range from the tangible (real estate and hardware) to the intangible (contracts and intellectual property). Hyperledger Fabric provides the ability to modify assets using chaincode transactions."

* [General concepts](https://hyperledger-fabric.readthedocs.io/en/latest/blockchain.html)
* [Install pre requirements](#setup-the-development-environment)
* [Install application dependencies](#install-dependencies)
* [Submit invoke transactions](#submit-invoke-transactions)
* [Submit query transactions](#submit-query-transactions)

Setup the development environment
----------------------------------

Install the [pre-requisites](https://hyperledger-fabric.readthedocs.io/en/latest/prereqs.html) like docker, nodejs and npm.

You must have a [basic network](https://github.com/hyperledger/fabric-samples/tree/release-1.4/basic-network) up and running,
so the application can be able to submit transactions to peers in the network.

As the application requires a `` DEFAULT_CONTRACT `` environment variable to be set,
install and instantiate your chaincode into network.
( In future versions it will handle chaincode identifiers from requests )


Install application dependencies
---------------------------------

From the root of this repository:

1. install npm modules running `` npm install ``
2. add an `` .env `` file with the following properties:
``` PORT=4000
    MONGO_API_KEY='some_key'
    MONGO_DB='some_db_name'
    MONGO_HOST='some_host'
    MAX_ENROLLMENTS=3
    ENROLLMENT_ID='user8'
    DEFAULT_ROLE='partner'
    ENROLLMENT_SECRET=null
    DEFAULT_CONTRACT='fabcar'
    DEFAULT_ATTR_VALUE='true'
    DEFAULT_ATTR_NAME='manager'
    DEFAULT_CHANNEL='mychannel'
    ORGANIZATION='org1.example.com'
    DEFAULT_AFFILIATION='org1.department1'
    FABRIC_ADMIN_HOST='http://localhost:3000'
```
    
3. run your application `` npm start ``

At this level your application has requested and imported into the wallet a pair of certificates. The [fabric-admin-service](https://gitlab.com/blockchain-in-a-nutshell/fabric-admin-service) is responsible to interface those requests, receiving and forwarding it to organization's certificate authority.