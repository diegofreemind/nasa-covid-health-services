const state = require('../providers/context');
const queryProvider = require('../providers/query');
const invokeProvider = require('../providers/invoke');

exports.invoke = async (params, user) => {

    try {

        const { contract, gateway } = await state.context(user);
        let res = invokeProvider(contract, params);

        // state.disconnect(gateway);
        return res;

    } catch (err) {
        return new Error(err.message);
    }
}

exports.query = async (params, user) => {

    try {

        const { contract, gateway } = await state.context(user);
        let res = queryProvider(contract, params);

        // state.disconnect(gateway);
        return res;

    } catch (err) {
        return new Error(err.message);
    }
}