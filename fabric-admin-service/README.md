fabric admin service
[<img src="https://hyperledger-fabric.readthedocs.io/en/latest/_images/hyperledger_fabric_logo_color.png" align="right"
alt="Discover the hyperledger project">](https://hyperledger-fabric.readthedocs.io/en/latest/)
==================

A micro service to interface certificate authority features to applications.
Each participant in the network should be representated by it's service.

official:
https://fabric-sdk-node.github.io/release-1.4/FabricCAServices.html


Setup the development environment
----------------------------------

Install the [pre-requisites](https://hyperledger-fabric.readthedocs.io/en/latest/prereqs.html) like docker, nodejs and npm.

You must have a [basic network](https://github.com/hyperledger/fabric-samples/tree/release-1.4/basic-network) up and running,
so the application can be able to interace certificate authority from network.

Install application dependencies
---------------------------------

From the root of this repository:

1. install npm modules running `` npm install ``
2. add an `` .env `` file with the following properties:
    ``` 
        PORT=3000
        ENROLLMENT_ID='admin'
        ENROLLMENT_SECRET='adminpw'
3. run your application `` npm start ``

It should register, enroll your admin user and import it's certificates into wallet.
At this level, your application should be able to receive requests to `` /api/auth `` so
it can register and enroll other users as well into the network.