const cloud_directory = require('../model/cloud_directory');
const context = require('../providers/users');
const mongodb = require('../model/mongo');

//TODO orchestrate cloud_directory features and mongo features

const enrollment = async (user) => {

    try {

        let certs = await context.register(user);
        return certs;

    } catch (err) {
        return new Error(err.message);
    }
}

const revoke = async (user) => {

    try {

        let revokeResults = await context.unregister(user);
        return revokeResults;

    } catch (err) {
        return new Error(err.message);
    }
}

module.exports = {

    register: enrollment,
    unregister: revoke

}