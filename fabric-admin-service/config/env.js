require('dotenv').config();

const cloud_directory_keys = require(`${process.env.CLOUD_DIRECTORY}`);

if (!process.env.ENROLLMENT_SECRET || !process.env.ENROLLMENT_ID) {
    throw new Error('Missing enrollment configurations');
}

module.exports = {

    enrollment: {
        id: process.env.ENROLLMENT_ID,
        secret: process.env.ENROLLMENT_SECRET
    },
    mongodb: {
        host: process.env.MONGO_HOST,
        database: process.env.MONGO_DB,
        api_key: process.env.MONGO_API_KEY
    },
    credentials: cloud_directory_keys,
    volume_path: process.env.VOLUME,
    server_port: process.env.PORT,
    msp: process.env.MSP
}
