const request = require('request-promise');
const { credentials } = require('../config/env');

exports.createUser = async (user) => {

    try {

        const { managementUrl } = credentials;

        let params = {
            method: 'POST',
            uri: `${managementUrl}/cloud_directory/Users`,
            headers: {
                'Accept': 'application/json',
                'Authorization': credentials.token
            },
            body: user,
            json: true
        };

        let userIdentity = await request(params);

        return userIdentity;

    } catch (err) {

        console.log(err.stack);
        return new Error(err.message);
    }

}

exports.getUserProfile = async (id) => {

    try {

        const { managementUrl } = credentials;

        let params = {
            method: 'GET',
            uri: `${managementUrl}/cloud_directory/Users/${id}`,
            headers: {
                'Accept': 'application/json',
                'Authorization': credentials.token
            },
            json: true
        };

        let userIdentity = await request(params);

        return userIdentity;

    } catch (err) {

        console.log(err.stack);
        return new Error(err.message);
    }
}

exports.updateUser = async (id, body) => {

    try {

        const { managementUrl } = credentials;

        let params = {
            method: 'PUT',
            uri: `${managementUrl}/cloud_directory/Users/${id}`,
            headers: {
                'Accept': 'application/json',
                'Authorization': credentials.token
            },
            body: body,
            json: true
        };

        let userIdentity = await request(params);

        return userIdentity;

    } catch (err) {

        console.log(err.stack);
        return new Error(err.message);
    }
}


exports.deleteUser = async (id) => {

    try {

        const { managementUrl } = credentials;

        let params = {
            method: 'DELETE',
            uri: `${managementUrl}/cloud_directory/Users/${id}`,
            headers: {
                'Accept': 'application/json',
                'Authorization': credentials.token
            },
            json: true
        };

        let deletedIdentity = await request(params);

        return deletedIdentity;

    } catch (err) {

        console.log(err.stack);
        return new Error(err.message);
    }
}