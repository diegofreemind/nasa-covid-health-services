//http://mongodb.github.io/node-mongodb-native/3.1/api/
const Mongo = require('mongodb');
const env = require('../config/env');

const mongodb = (async () => {

    try {

        let connection = await Mongo.connect(
            env.mongodb.host,
            { useNewUrlParser: true }
        );

        return connection.db(env.mongodb.database);

    } catch (err) {
        throw new Error(err.message);
    }

})();


/** 
 * http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html
 * Mongo DB collection operations reference
*/

/**
 * Creates a document
 * http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#save
 * @param payload the collection and document to be saved
 * @returns {Promise} A Promise holding a single document
 */
exports.save = async (payload) => {

    try {

        let { collection, document } = payload;
        let instance = await mongodb;
        let target = instance.collection(collection);

        return target.insertOne(document);

    } catch (error) {
        return new Error(error.message);
    }
};

/**
 * Finds a document
 * http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#findOne
 * @param payload the collection and id of the document to be retrieved
 * @returns {Promise} A Promise with a single document
 */
exports.findOne = async (payload) => {

    try {

        let { _id, collection } = payload;
        let instance = await mongodb;
        let target = instance.collection(collection);

        return target.findOne({ _id });

    } catch (error) {
        return new Error(error.message);
    }
};

/**
 * Finds a set of documents
 * http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#find
 * @param payload the collection and query to find documents
 * @returns {Promise} A Promise with a set of documents
 */
exports.find = async (payload) => {

    try {

        let { collection, query } = payload;
        let instance = await mongodb;
        let target = instance.collection(collection);

        return target.find(query).toArray();

    } catch (error) {
        return new Error(error.message);
    }
};

/**
 * Updates a document
 * http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#updateOne
 * @param payload the collection and criteria to delete a document
 * @returns {Promise} A Promise with the deletion comfirmation
 */
exports.updateOne = async (payload) => {

    try {

        let { collection, query, document } = payload;
        let instance = await mongodb;
        let target = instance.collection(collection);

        return target.updateOne(query, document);

    } catch (error) {
        return new Error(error.message);
    }
};

/**
 * Deletes a document
 * http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#deleteOne
 * @param payload the collection and criteria to delete a document
 * @returns {Promise} A Promise with the deletion comfirmation
 */
exports.delete = async (payload) => {

    try {

        let { collection, query } = payload;
        let instance = await mongodb;
        let target = instance.collection(collection);

        return target.deleteOne(query);

    } catch (error) {
        return new Error(error.message);
    }
};
