const express = require('express');
const env = require('./config/env');
const apiRouter = require('./routes/api');
const setAdminWallet = require('./providers/admin');

const app = express();

app.use(express.json());
let morgan = require('morgan');

app.use(morgan('combined'));

app.use(express.urlencoded({ extended: false }));
app.use('/api', apiRouter);

app.listen(env.server_port);
module.exports = app;