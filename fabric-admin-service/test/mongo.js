const chai = require('chai');
const chaiHttp = require('chai-http');
const mongodb = require('../model/mongo');

chai.use(chaiHttp);
const assert = chai.assert;

describe('mongodb operations', function () {

    let documentId = null;
    const alias = new Date().getMilliseconds().toString();

    describe('#save()', function () {

        let document = {
            emails:
                [{
                    value: 'user1@org1.example.com',
                    primary: true
                }],
            password: alias + '+labs+garage',
            userName: alias + '+labs+garage',
            active: true,
            certificates: {
                type: "X509",
                mspId: "Org1MSP",
                certificate: "-----BEGIN CERTIFICATE-----\nMIICxzCCAm...fpyBcA==\n-----END CERTIFICATE-----\n",
                privateKey: "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAM...zSmm9\r\n-----END PRIVATE KEY-----\r\n"
            },
            identity: "user1@org1.example.com"
        };

        this.timeout(60000);

        it('should return a Promise', function (done) {

            let payload = {
                document,
                collection: 'Users'
            };

            mongodb.save(payload)
                .then(res => {

                    console.log(res);

                    assert.deepEqual(res.result, { ok: 1, n: 1 });
                    documentId = res.ops[0]._id;
                    done();
                })
                .catch(err => done(err.message))
        });
    });

    describe('#findOne()', function () {

        this.timeout(60000);

        it('should return a Promise<Document>', function (done) {

            let payload = {
                _id: documentId,
                collection: 'Users'
            };

            mongodb.findOne(payload)
                .then(res => {

                    console.log(res);

                    assert.deepEqual(res._id, documentId);
                    done();
                })
                .catch(err => done(err.message))
        });
    });


    describe('#findMany()', function () {

        this.timeout(60000);

        it('should return a Promise<Document[]>', function (done) {

            let payload = {
                query: {
                    userName: alias + '+labs+garage'
                },
                collection: 'Users'
            };

            mongodb.find(payload)
                .then(res => {

                    console.log(res);

                    assert.isArray(res, 'find() returned an array');
                    done();
                })
                .catch(err => done(err.message))
        });
    });

});
