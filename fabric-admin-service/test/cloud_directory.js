const chai = require('chai');
const alias = new Date().getMilliseconds().toString();
const cloudDirectory = require('../model/cloud_directory');

const assert = chai.assert;

describe('Execute cloud directory operations', function () {

    describe(`create and update user in cloud directory`, function () {

        let user = {
            emails:
                [{
                    value: alias + '@org1.example.com',
                    primary: true
                }],
            password: alias + '+labs+garage',
            userName: alias + '+labs+garage',
            active: true
        };

        let newUser = null;

        // Check to see if we've already created this user
        it(`#createUser() => userIdentity`, async () => {

            try {

                newUser = await cloudDirectory.createUser(user);
                assert.isObject(newUser, 'newUser should be an object');

                console.log(newUser);

            } catch (error) {
                return error;
            }
        });

        // Check to see if we've already created this user
        it(`#getUserProfile() => userIdentity`, async () => {

            try {

                let { id } = newUser;

                let userProfile = await cloudDirectory.getUserProfile(id);
                assert.isObject(userProfile, 'userProfile should be an object');

                console.log(userProfile);

            } catch (error) {
                return error;
            }
        });

        // Check to see if we've already created this user
        it(`#updateUser() => userIdentity`, async () => {

            try {

                let { id } = newUser;
                user.active = false;

                let updateUser = await cloudDirectory.updateUser(id, user);
                assert.isObject(updateUser, 'updateUser should be an object');

                console.log(updateUser);

            } catch (error) {
                return error;
            }
        });

        // Check to see if we've already created this user
        it(`#deleteUser() => deletedIdentity`, async () => {

            try {

                let { id } = newUser;

                let deletedIdentity = await cloudDirectory.deleteUser(id);
                assert.isObject(deletedIdentity, 'deletedIdentity should be an object');

                console.log(deletedIdentity);

            } catch (error) {
                return error;
            }
        });

    });
});