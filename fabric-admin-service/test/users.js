const chai = require('chai');
const userProvider = require('../providers/users');
const { Gateway } = require('fabric-network');

const assert = chai.assert;

describe('Execute admin operations', function () {

    describe(`revogation flow`, function () {

        let ca = null;
        let gateway = null;
        let adminIdentity = null;

        const user = {
            enroll_id: 'user2@org1.example.com'
        };

        const revokeDates = {
            revokedBefore: new Date(),
            revokedAfter: new Date(),
            expireBefore: new Date(),
            expireAfter: new Date()
        }

        // Check to see if we've already enrolled the admin.
        it(`#admin.session() => Gateway`, async () => {

            try {

                const wallet = await userProvider.getWallet();
                let validSession = await userProvider.ensureAdminSession(wallet)

                if (!validSession) {
                    return new Error('Admin not enrolled');
                }

                // Create a new gateway for connecting to our peer node.
                gateway = await userProvider.getGateway('admin', wallet);

                assert.instanceOf(gateway, new Gateway());

            } catch (error) {
                return error;
            }
        });

        it(`#getCertificateAuthority() &&  getAdminIdentity()`, async () => {

            try {

                ca = await userProvider.getCertificateAuthority(gateway);
                adminIdentity = await userProvider.getAdminIdentity(gateway);

                assert.isNotNull(ca);
                assert.isNotNull(adminIdentity);

            } catch (error) {
                return error;
            }
        });

        it(`#revokeUser()`, async () => {

            try {

                const revokeResults = await userProvider.revokeUser(user, adminIdentity, ca);
                console.log(revokeResults)

                return revokeResults;

            } catch (error) {
                return error;
            }
        });

        it(`#publishRevoke()`, async () => {

            try {

                const revokeResults = await userProvider.publishRevoke(revokeDates, adminIdentity, ca);
                console.log(revokeResults)

                return revokeResults;

            } catch (error) {
                return error;
            }
        });
    });
});