/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { FileSystemWallet, Gateway, X509WalletMixin } = require('fabric-network');
const ccp = require('../config/network.json');
const env = require('../config/env');
const path = require('path');

async function addUser(user) {
    try {

        const wallet = await getWallet();
        let validSession = await ensureAdminSession(wallet);

        if (!validSession) {

            return new Error('Admin not enrolled');
        }

        // Check to see if we've already enrolled the user.
        console.log(await wallet.getAllLabels())
        const userExists = await wallet.exists(user.enroll_id);
        if (userExists) {
            console.log(`An identity for the user ${user.enroll_id} already exists in the wallet`);
            return new Error(`User ${user.enroll_id} already registered`);
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = await getGateway('admin', wallet);

        // Get the CA client object from the gateway for interacting with the CA.
        const ca = await getCertificateAuthority(gateway);
        const adminIdentity = await getAdminIdentity(gateway);

        // Register the user, enroll the user, and import the new identity into the wallet.
        const userKey = await registerUser(user, adminIdentity, ca);
        const enrollment = await enrollUser(user.enroll_id, userKey, ca);

        const userIdentity = await createIdentity(enrollment);
        await wallet.import(user.enroll_id, userIdentity);
        return userIdentity;

    } catch (err) {
        console.error(`Failed to register user ${user.enroll_id}: ${err}`);
        return new Error(err.message);
    }
}

async function removeUser(user) {
    try {
        const wallet = await getWallet();
        let validSession = await ensureAdminSession(wallet);

        if (!validSession) {
            return new Error('Admin not enrolled');
        }
        console.log(await wallet.getAllLabels())
        const userExists = await wallet.exists(user.enroll_id);
        if (!userExists) {
            console.log(`The user ${user.enroll_id} is not present in the wallet`);
            return new Error(`User ${user.enroll_id} not registered`);
        }
        // Create a new gateway for connecting to our peer node.
        const gateway = await getGateway('admin', wallet);
        // Get the CA client object from the gateway for interacting with the CA.
        const ca = await getCertificateAuthority(gateway);
        const adminIdentity = await getAdminIdentity(gateway);
        const revokeResults = await revokeUser(user, adminIdentity, ca);
        console.log(user)
        await wallet.delete(user.enroll_id);
        return revokeResults;

    } catch (err) {
        console.error(`Failed to revoke user ${user.enroll_id}: ${err}`);
        return new Error(err.message);
    }
}

async function revokeUser(user, adminIdentity, ca) {
    return ca.revoke({
        enrollmentID: user.enroll_id
    }, adminIdentity);
}

async function publishRevoke(restriction, user, ca) {

    return ca.generateCRL(restriction, user);
}

async function getWallet() {

    const mountPath = env.volume_path || process.cwd();
    const walletPath = path.join(mountPath, 'wallet');

    const wallet = new FileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    return wallet;
}

async function getAdminIdentity(gateway) {

    const adminIdentity = gateway.getCurrentIdentity();

    return adminIdentity;
}

async function getCertificateAuthority(gateway) {

    // Get the CA client object from the gateway for interacting with the CA.
    const ca = gateway.getClient().getCertificateAuthority();
    return ca;
}

async function ensureAdminSession(wallet) {

    // Check to see if we've already enrolled the admin user.
    const adminExists = await wallet.exists('admin');

    return adminExists;
}

async function getGateway(id, wallet) {

    try {

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: id, discovery: { enabled: false } });

        return gateway;

    } catch (error) {

        return new Error(error);
    }

}

async function createIdentity(enrollment) {
    return X509WalletMixin.createIdentity(env.msp, enrollment.certificate, enrollment.key.toBytes());
}

//https://fabric-sdk-node.github.io/release-1.4/global.html#RegisterRequest
async function registerUser(user, adminIdentity, ca) {

    return ca.register({
        role: user.role,
        attrs: user.attrs,
        enrollmentID: user.enroll_id,
        affiliation: user.affiliation,
        maxEnrollments: user.maxEnrollments
    }, adminIdentity);
}

async function enrollUser(enroll_id, secret, ca) {

    return ca.enroll({ enrollmentID: enroll_id, enrollmentSecret: secret });
}

module.exports = {
    register: addUser,
    enroll: enrollUser,
    unregister: removeUser,
    getWallet,
    revokeUser,
    getGateway,
    publishRevoke,
    getAdminIdentity,
    ensureAdminSession,
    getCertificateAuthority
};