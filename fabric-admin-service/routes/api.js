const express = require('express');

const router = express.Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../config/swagger.json');
const controller = require('../controller/api-controller');

router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument));

router.post('/registrar', (req, res, next) => {

    const payload = req.body;

    controller.register(payload)
        .then((enrollment) => {

            res.send(enrollment);

        }).catch(next);
});

router.post('/revoke', (req, res, next) => {

    controller.unregister(req.body)
        .then((revoke) => {

            res.send(revoke);

        }).catch(next);
});

router.use((err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send(err.message);
});


module.exports = router;